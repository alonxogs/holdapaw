const express = require('express');
const router = express.Router();

//CONTROLLERS
const userController = require('../controllers/users');

//HELPERS
const uploadMiddleware = require('../helpers').upload;
const checkLogin = require('../controllers/middlewares').checkLogin;

router.route('/')
  .get(checkLogin, userController.getAllUsers)
  .post(uploadMiddleware.single('photo'), userController.createUser);


/*router.get('/asdfg', function(req ,res) {
  var User = require('../models/user');
  var user =  new User({
    dni: '71205418',
    firstName: 'Alonso',
    lastName: 'Godinez',
    email: 'alonxogs@gmail.com',
    type: 'activist',
    isAdmin: true,
    verified: true
    cellphone: '931850120',
    birthday: new Date('04-01-1995'),
    photo: "https://api-content.dropbox.com/1/files/auto/photo-1465427824328.png?access_token=DFYRE-WjEHYAAAAAAAAALAsuXBqyTFLdPiMJVMg6DGvCZ9kGTrET35OJ53j3Obne",
    photoThumb: "https://api-content.dropbox.com/1/thumbnails/auto/photo-1465427824328.png?access_token=DFYRE-WjEHYAAAAAAAAALAsuXBqyTFLdPiMJVMg6DGvCZ9kGTrET35OJ53j3Obne"
  });
  user.password = user.generateHash ('123456');
  user.save().then(res.sendStatus(200)).catch(res.sendStatus(503))
});*/

router.route ('/perfil/:id').get(userController.getOne);
module.exports = router;